const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const app = Byrds(conf.apiKeys.client)

describe('app.devices', () => {

	// crudTests.expectCRUD(app.devices)

	xit ('.read() should return a device', done => {
		app.devices.read().then(devices => {
			expect(devices instanceof Array).toBe(true)
			expect(devices.length).toEqual(1)

			var d = devices[0]
			expect(d.user.id).toEqual(user.data.id)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})