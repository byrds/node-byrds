const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')
const jwt = require('jsonwebtoken')
const randomkey = require('randomkey')

const byrds = Byrds(conf.apiKeys.client)

describe('user.org(:id).users', () => {

	var org = undefined
	var admin = undefined
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			admin = u
			return admin.orgs.create({ name: randomkey(16) })
		}).then(o => {
			org = o
		}).then(done)
	})

	crudTests.expectCRUD(_ => admin.org(org.id).users)

	describe('.create()', () => {

		var employee = conf.users.random()
		it ('should create a user', done => {
			admin.org(org.id).users.create({
				name: employee.name,
				email: employee.email,
				password: employee._pwd
			}).then(user => {
				expect(typeof user).toEqual('object')
				expect(user.name).toEqual(employee.name)
				expect(user.email).toEqual(employee.email)

				employee.id = user.id
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('who should be able to login', done => {
			byrds.login(employee).then(user => {
				expect(typeof user).toEqual('object')
				expect(typeof user.userData).toEqual('object')
				expect(user.userData.name).toEqual(employee.name)
				expect(user.userData.email).toEqual(employee.email)

				const token = user.withContext({ _exposeInternal: true }).context.userToken
				expect(typeof token).toEqual('string')
				var decoded = jwt.decode(token, { complete: true })
				expect(decoded.header.typ).toEqual('JWT')
				expect(decoded.header.alg).toEqual('HS256')
				expect(decoded.payload.iss).toEqual(user.userData.id)
				expect(decoded.payload.username).toEqual(employee.name)

				// Scoped IDs
				expect(user.userData.id).not.toEqual(employee.id)
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

	})

	describe('.read()', () => {

		xit ('should list users', done => {})
		xit ('should all belong to the correct organization', done => {})

	})

	describe('.read(:id)', () => {

		xit ('should list a user', done => {})
		xit ('should belong to the correct organization', done => {})
		xit ('should return 404 if not part of the organization', done => {})
		xit ('should return 404 if id is invalid', done => {})

	})

	describe('.update()', () => {

		xit ('should update a user', done => {})

	})

	describe('.delete()', () => {

		xit ('should delete a user', done => {})

	})

})
