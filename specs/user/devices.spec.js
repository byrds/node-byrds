const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

describe('user.devices', () => {

	var user = undefined
	var deviceIds = undefined
	var deviceCount = getRandomInt(5, 15)
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
		}).then(_ => {
			return conf.devices.insertMany(user, deviceCount)
		}).then(_ => {
			return user.devices.read({ pageSize: 500 })
		}).then(devices => {
			deviceIds = devices.map(d => d.id)
		}).then(done)
	})

	crudTests.expectCRUD(_ => user.devices, _ => deviceIds)

	describe('.read()', () => {

		it ('should return correct output (' + deviceCount + ' devices)', done => {
			user.devices.read({ pageSize: 500 }).then(devices => {
				expect(devices instanceof Array).toBe(true)
				expect(devices.length).toEqual(deviceCount)

				var d = devices[0]
				expect(d.id).toBeDefined()
				expect(d.udid).toBeDefined()
				expect(d.serial).toBeDefined()
			}).catch(err => {
			/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('should return user\'s devices only', done => {
			user.devices.read().then(devices => {
				devices.forEach(d => expect(d.user.id).toEqual(user.userData.id))
			}).catch(err => {
			/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

	})

})

describe('user.device(:id)', () => {

	var user = undefined
	const device = conf.devices.random()
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
			return user.devices.create(device)
		}).then(d => {
			device.id = d.id
		}).then(done)
	})

	it ('.get(:id) should return the correct device', done => {
		// user.device(device.id).get().then(d => {
		user.devices.get(device.id).then(d => {
			expect(typeof d).toEqual('object')

			expect(d.id).toEqual(device.id)
			expect(d.serial).toEqual(device.serial)
			expect(d.meta.name).toEqual(device.meta.name)
			expect(d.user.id).toEqual(user.userData.id)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	xit ('.retire() should retire a device', done => {
		user.devices.read({ limit: 1 }).then(devices => {
			return devices[0]
		}).then(device => {
			expect(device.status).toEqual('ENROLMENT_STATUS_ENROLLED')
			// return device.update({
			return [device, user.device(device.id).retire()]
		}).spread((device, result) => {
			expect(result.acknowledged).toBe(true)
			return user.device(device.id).devices.read()
		}).then(device => {
			expect(device.status).toEqual('ENROLMENT_STATUS_RETIRED')
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
