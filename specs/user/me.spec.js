const Byrds = require('../../main')
const conf = require('../helpers/config')
const jwt = require('jsonwebtoken')

const byrds = Byrds(conf.apiKeys.client)

describe('user.me.info()', () => {

	var user = undefined
	const data = conf.users.random()
	beforeAll(done => {
		byrds.signup(data).then(u => {
			user = u
			data.id = user.userData.id
		}).then(done)
	})

	it ('should return a Promise', () => {
		const p = byrds.me.info()
		expect(typeof p.then).toEqual('function')
	})

	it ('should return correct output', done => {
		user.me.info().then(u => {
			expect(typeof u).toEqual('object')
			expect(u.id).toEqual(data.id)
			expect(u.name).toEqual(data.name)
			expect(u.email).toEqual(data.email)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should not return unsafe properties', done => {
		user.me.info().then(u => {
			expect(u.password).toBeUndefined()
			expect(u._pwd).toBeUndefined()
			expect(u._pwdSalt).toBeUndefined()
			expect(u._jwtSalt).toBeUndefined()
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
