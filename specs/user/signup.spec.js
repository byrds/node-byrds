const Byrds = require('../../main')
const conf = require('../helpers/config')
const jwt = require('jsonwebtoken')

const byrds = Byrds(conf.apiKeys.client)

describe('byrds.signup()', () => {

	it ('should return a Promise', () => {
		const d = conf.users.random()
		const p = byrds.signup(d.name, d.email, d.password)
		expect(typeof p.then).toEqual('function')
	})

	const data = conf.users.random()
	it ('should create a user', done => {
		byrds.signup(data).then(user => {
			expect(typeof user).toEqual('object')
			expect(typeof user.userData).toEqual('object')
			expect(user.userData.name).toEqual(data.name)
			expect(user.userData.email).toEqual(data.email)

			data.id = user.userData.id
		}).then(done)
	})

	it ('should be able to login', done => {
		byrds.login(data).then(user => {
			expect(typeof user).toEqual('object')
			expect(typeof user.userData).toEqual('object')
			expect(user.userData.id).toEqual(data.id)
		}).then(done)
	})

	it ('should return a valid JWT', done => {
		const tmpData = conf.users.random()
		byrds.signup(tmpData).then(user => {
			const token = user.withContext({ _exposeInternal: true }).context.userToken
			expect(typeof token).toEqual('string')
			var decoded = jwt.decode(token, { complete: true })
			expect(decoded.header.typ).toEqual('JWT')
			expect(decoded.header.alg).toEqual('HS256')
			expect(decoded.payload.iss).toEqual(user.userData.id)
			expect(decoded.payload.username).toEqual(tmpData.name)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should not return unsafe properties', done => {
		const tmpData = conf.users.random()
		byrds.signup(tmpData).then(user => {
			expect(user.userData.password).toBeUndefined()
			expect(user.userData._pwd).toBeUndefined()
			expect(user.userData._pwdSalt).toBeUndefined()
			expect(user.userData._jwtSalt).toBeUndefined()
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})

xdescribe('byrds.signup(): Invalid', () => {

	xit ('should return 400', done => {
		const data = conf.users.random()
		data.email = 'invalid_email'
		byrds.signup(data).then(u => {
			/* istanbul ignore next  */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(400, err)
		}).then(done)
	})

	xit ('should return an error description', done => {
		const data = conf.users.random()
		data.email = 'invalid_email'
		byrds.signup(data).then(u => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.data.error).toEqual('Field \'' + user.userData.email + '\' is invalid')
		}).then(done)
	})

})

describe('byrds.signup(): Already exists', () => {

	var user = undefined
	beforeAll(done => {
		const data = conf.users.random()
		byrds.signup(data).then(u => {
			user = u
		}).then(done)
	})

	it ('should return a 400', done => {
		byrds.signup(user.userData).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(400, err)
		}).then(done)
	})

	it ('should return not return a user', done => {
		var empty = undefined
		byrds.signup(user.userData).then(u => {
			empty = u
		}).catch(err => {
			// Skip this
		}).then(_ => {
			expect(empty).toBeUndefined()
			done()
		})
	})
	
	it ('should return an error description', done => {
		byrds.signup(user.userData).then(user => {
			/* istanbul ignore next */
			throw new Error('Return 200')
		}).catch(err => {
			expect(err.data.error).toEqual('User email already taken')
		}).then(done)
	})

	it ('should let the original user untouched', done => {
		user.me.info().then(info => {
			expect(info.id).toEqual(user.userData.id)
			expect(info.email).toEqual(user.userData.email)
			expect(info.name).toEqual(user.userData.name)
		}).catch(err => {
			expect(err.status).toEqual(400, err)
		}).then(done)
	})

})
