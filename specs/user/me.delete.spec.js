const Byrds = require('../../main')
const conf = require('../helpers/config')

const byrds = Byrds(conf.apiKeys.client)

describe('user.me.delete()', () => {

	var user = undefined
	var data = undefined
	beforeEach(done => {
		data = conf.users.random()
		byrds.signup(data).then(u => {
			user = u
			data.id = user.userData.id
		}).then(done)
	})

	it ('should return a Promise', () => {
		const p = byrds.me.delete()
		expect(typeof p.then).toEqual('function')
	})

	it ('should return correct output', done => {
		user.me.delete().then(response => {
			expect(typeof response).toEqual('object')
			expect(response.success).toEqual(true)
		}).catch(err => {
			console.log('err:', err)
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should not be able to login again', done => {
		user.me.delete().then(response => {
			return byrds.login(data).then(user => {
				/* istanbul ignore next */
				throw new Error('Returned 200')
			}).catch(err => {
				expect(err.status).toEqual(401, err)
			})
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should be able to signup again', done => {
		user.me.delete().then(response => {
			return byrds.signup(data).then(user => {
				expect(typeof user).toEqual('object')
				expect(typeof user.userData).toEqual('object')
				expect(user.userData.name).toEqual(data.name)
				expect(user.userData.email).toEqual(data.email)
			})
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
