const Byrds = require('../../main')
const conf = require('../helpers/config')

const byrds = Byrds(conf.apiKeys.client)

function getRandomArbitrary(min, max) {
	return Math.floor(Math.random() * (max - min) + min)
}

describe('user.me.edit(:data)', () => {

	var user = undefined
	var data = undefined
	beforeEach(done => {
		data = conf.users.random()
		byrds.signup(data).then(u => {
			user = u
			data.id = user.userData.id
		}).then(done)
	})

	it ('should return a Promise', () => {
		const p = byrds.me.edit({})
		expect(typeof p.then).toEqual('function')
	})

	it ('should return correct output', done => {
		data.name = 'John Doe'
		user.me.edit({
			name: data.name,
			email: data.email,
		}).then(u => {
			expect(typeof u).toEqual('object')
			expect(u.id).toEqual(data.id)
			expect(u.name).toEqual(data.name)
			expect(u.email).toEqual(data.email)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should require the current password if modifying password', done => {
		data.currentPassword = undefined
		data.password = 'newPassword'
		user.me.edit({
			password: data.password,
		}).then(u => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(400, err)
			expect(err.data.error).toEqual('Modifying password require the \'currentPassword\' field')
		}).then(done)
	})

	it ('should disable current (JWT) auth token', done => {
		data.currentPassword = data.password
		data.password = 'newPassword'
		user.me.edit({
			password: data.password,
			currentPassword: data.currentPassword,
		}).then(u => {
			return user.me.info(data).then(user => {
				/* istanbul ignore next */
				throw new Error('Returned 200')
			}).catch(err => {
				expect(err.status).toEqual(401, err)
			})
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should not be able to login with old password', done => {
		data.currentPassword = data.password
		data.password = 'newPassword'
		user.me.edit({
			password: data.password,
			currentPassword: data.currentPassword,
		}).then(u => {
			data.password = data.currentPassword
			return byrds.login(data).then(user => {
				/* istanbul ignore next */
				throw new Error('Returned 200')
			}).catch(err => {
				expect(err.status).toEqual(401, err)
			})
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should be able to login with new password', done => {
		data.currentPassword = data.password
		data.password = 'newPassword'
		user.me.edit({
			password: data.password,
			currentPassword: data.currentPassword,
		}).then(u => {
			return byrds.login(data).then(user => {
				expect(typeof u).toEqual('object')
				expect(u.id).toEqual(data.id)
				expect(u.name).toEqual(data.name)
				expect(u.email).toEqual(data.email)
			})
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})

describe('user.me.edit(:data): Auhty enabled', () => {

	var user = undefined
	var data = undefined
	const validAuthyToken = '0000000'
	beforeAll(done => {
		data = conf.users.random()
		byrds.signup(data).then(u => {
			user = u
			data.id = user.userData.id
		}).then(_ => {
			return user.twoFactorAuth.authy.register({
				email: data.email,
				countryCode: 'FR',
				phone: '06' + getRandomArbitrary(10000000, 99999999)
			}).then(u => {
				return user.twoFactorAuth.authy.validate(validAuthyToken)
			})
		}).then(done)
	})

	it ('should require a token if modifying password and 2FA is enabled', done => {
		data.currentPassword = data.password
		data.password = 'newPassword'
		data.token = validAuthyToken
		user.me.edit({
			token: data.token,
			password: data.password,
			currentPassword: data.currentPassword,
		}).then(u => {
			return byrds.login(data).then(user => {
				expect(typeof u).toEqual('object')
				expect(u.id).toEqual(data.id)
				expect(u.name).toEqual(data.name)
				expect(u.email).toEqual(data.email)
			})
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should require a token if modifying password and 2FA is enabled: no token', done => {
		data.currentPassword = data.password
		data.password = 'newPassword'
		user.me.edit({
			password: data.password,
			currentPassword: data.currentPassword,
		}).then(u => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
		}).then(done)
	})

	it ('should require a token if modifying password and 2FA is enabled: wrong token', done => {
		data.currentPassword = data.password
		data.password = 'newPassword'
		user.me.edit({
			token: 'invalid_token',
			password: data.password,
			currentPassword: data.currentPassword,
		}).then(u => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
		}).then(done)
	})

})
