const Byrds = require('../../main')
const conf = require('../helpers/config')
const jwt = require('jsonwebtoken')

const byrds = Byrds(conf.apiKeys.client)

describe('byrds.login()', () => {

	const data = conf.users.random()
	beforeAll(done => {
		byrds.signup(data).then(user => {
			data.id = user.userData.id
		}).then(done)
	})

	it ('should return a Promise', () => {
		const p = byrds.login(data)
		expect(typeof p.then).toEqual('function')
	})

	it ('should be reported as authenticated', done => {
		byrds.login(data).then(user => {
			expect(user.isAuthenticated()).toEqual(true)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should return correct output', done => {
		byrds.login(data).then(user => {
			expect(typeof user).toEqual('object')
			expect(typeof user.userData).toEqual('object')
			expect(user.userData.id).toEqual(data.id)
			expect(user.userData.name).toEqual(data.name)
			expect(user.userData.email).toEqual(data.email)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should return a valid JWT', done => {
		byrds.login(data).then(user => {
			const token = user.withContext({ _exposeInternal: true }).context.userToken
			expect(typeof token).toEqual('string')
			var decoded = jwt.decode(token, { complete: true })
			expect(decoded.header.typ).toEqual('JWT')
			expect(decoded.header.alg).toEqual('HS256')
			expect(decoded.payload.iss).toEqual(user.userData.id)
			expect(decoded.payload.username).toEqual(data.name)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should not return unsafe properties', done => {
		byrds.login(data).then(user => {
			expect(user.userData.password).toBeUndefined()
			expect(user.userData._pwd).toBeUndefined()
			expect(user.userData._pwdSalt).toBeUndefined()
			expect(user.userData._jwtSalt).toBeUndefined()
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})

describe('byrds.login(): Wrong password', () => {

	const data = conf.users.random()
	beforeAll(done => {
		byrds.signup(data).then(user => {
			data.id = user.userData.id
			data.password = 'WRONG_PASSWORD'
		}).then(done)
	})

	it ('should return a 401', done => {
		byrds.login(data).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
		}).then(done)
	})

	it ('should return not return a user', done => {
		var empty = undefined
		byrds.login(data).then(user => {
			/* Should never happen */
			/* istanbul ignore next */
			empty = u
		}).catch(err => {
			// Skip this
		}).then(_ => {
			expect(empty).toBeUndefined()
			done()
		})
	})
	
	it ('should return an error description', done => {
		byrds.login(data).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.data.error).toEqual('Authentication did not succeed')
		}).then(done)
	})

})

describe('byrds.login(): Wrong email', () => {

	const data = conf.users.random()
	beforeAll(done => {
		byrds.signup(data).then(user => {
			data.id = user.userData.id
			data.password = 'WRONG_PASSWORD'
		}).then(done)
	})

	it ('should return a 401', done => {
		byrds.login(data).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
		}).then(done)
	})

	it ('should return not return a user', done => {
		var empty = undefined
		byrds.login(data).then(user => {
			/* Should never happen */
			/* istanbul ignore next */
			empty = u
		}).catch(err => {
			// Skip this
		}).then(_ => {
			expect(empty).toBeUndefined()
			done()
		})
	})
	
	it ('should return an error description', done => {
		byrds.login(data).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.data.error).toEqual('Authentication did not succeed')
		}).then(done)
	})

})
