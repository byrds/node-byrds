const Byrds = require('../../main')
const conf = require('../helpers/config')
const jwt = require('jsonwebtoken')

const byrds = Byrds(conf.apiKeys.client)

var data = conf.users.random()
data.token = '0000000'

function getRandomArbitrary(min, max) {
	return Math.floor(Math.random() * (max - min) + min)
}

describe('byrds.twoFactorAuth', () => {

	describe('.authy', () => {

		var user = undefined
		beforeAll(done => {
			byrds.signup(data).then(u => {
				data.id = u.userData.id
				user = u
			}).then(done)
		})

		describe('.register(:data)', () => {

			it ('should enable Two Factor Authentication, via Authy', done => {
				user.twoFactorAuth.authy.register({
					email: data.email,
					countryCode: 'FR',
					phone: '06' + getRandomArbitrary(10000000, 99999999)
				}).then(u => {
					expect(u.twoFactorAuth).toEqual('none')
					return user.twoFactorAuth.authy.validate(data.token)
				}).then(u => {
					expect(u.twoFactorAuth).toEqual('authy')
				}).catch(err => {
					/* istanbul ignore next */
					expect(err.status).toEqual(200, err)
				}).then(done)
			});  

		})

		describe('.register(:data): Not authenticated', () => {

			it ('should return an error', done => {
				byrds.twoFactorAuth.authy.register({
					email: data.email,
					countryCode: 'FR',
					phone: '06' + getRandomArbitrary(10000000, 99999999)
				}).then(_ => {
					throw new Error('Return 200')
				}).catch(err => {
					expect(err.status).toEqual(401, err)
				}).then(done)
			})

		})

	})

})

describe('byrds.login(): Authy enabled', () => {

	var user = undefined

	it ('should be reported as authenticated', done => {
		byrds.login(data).then(u => {
			user = u
			expect(user.isAuthenticated()).toEqual(true)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should return correct output', () => {
		expect(typeof user).toEqual('object')
		expect(typeof user.userData).toEqual('object')
		expect(user.userData.id).toEqual(data.id)
		expect(user.userData.name).toEqual(data.name)
		expect(user.userData.email).toEqual(data.email)
	})

	it ('should return a valid JWT', () => {
		const token = user.withContext({ _exposeInternal: true }).context.userToken
		expect(typeof token).toEqual('string')
		var decoded = jwt.decode(token, { complete: true })
		expect(decoded.header.typ).toEqual('JWT')
		expect(decoded.header.alg).toEqual('HS256')
		expect(decoded.payload.iss).toEqual(user.userData.id)
		expect(decoded.payload.username).toEqual(data.name)
	})

	it ('should not return unsafe properties', () => {
		expect(user.userData.password).toBeUndefined()
		expect(user.userData._pwd).toBeUndefined()
		expect(user.userData._pwdSalt).toBeUndefined()
		expect(user.userData._jwtSalt).toBeUndefined()
	})

})

describe('byrds.login(): Authy enabled: Missing token', () => {

	var loginData = {
		email: data.email,
		password: data.password
	}

	it ('should return a 401', done => {
		byrds.login(loginData).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
		}).then(done)
	})

	it ('should return not return a user', done => {
		var empty = undefined
		byrds.login(loginData).then(user => {
			/* Should never happen */
			/* istanbul ignore next */
			empty = u
		}).catch(err => {
			// Skip this
		}).then(_ => {
			expect(empty).toBeUndefined()
			done()
		})
	})
	
	it ('should return an error description', done => {
		byrds.login(loginData).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
			expect(err.data.error).toEqual('Two Factor Authentication is required for this account')
			expect(err.data.twoFactorAuth).toEqual('authy')
		}).then(done)
	})

})

describe('byrds.login(): Authy enabled: Wrong token', () => {

	var loginData = {
		email: data.email,
		password: data.password,
		token: 'invalid_token'
	}

	it ('should return a 401', done => {
		byrds.login(loginData).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
		}).then(done)
	})

	it ('should return not return a user', done => {
		var empty = undefined
		byrds.login(loginData).then(user => {
			/* Should never happen */
			/* istanbul ignore next */
			empty = u
		}).catch(err => {
			// Skip this
		}).then(_ => {
			expect(empty).toBeUndefined()
			done()
		})
	})
	
	it ('should return an error description', done => {
		byrds.login(loginData).then(user => {
			/* istanbul ignore next */
			throw new Error('Returned 200')
		}).catch(err => {
			expect(err.status).toEqual(401, err)
			expect(err.data.error).toEqual('Authentication did not succeed')
		}).then(done)
	})

})

describe('byrds.twoFactorAuth', () => {

	var user = undefined
	beforeAll(done => {
		byrds.login(data).then(u => {
			user = u
		}).catch(err => {
			console.log('err:', err)
		}).then(done)
	})

	describe('.disable()', () => {

		it ('should disable Two Factor Authentication', done => {
			user.twoFactorAuth.disable().then(u => {
				expect(u.twoFactorAuth).toEqual('none')
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('should be able to login again without a token', done => {
			var loginData = {
				email: data.email,
				password: data.password
			}
			byrds.login(loginData).then(u => {
				expect(u.isAuthenticated()).toEqual(true)
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

	})

})
