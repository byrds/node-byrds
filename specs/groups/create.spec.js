const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const byrds = Byrds(conf.apiKeys.client)

describe('org(:id).groups.create()', () => {

	var user = undefined
	var org = conf.orgs.random()
	var data = conf.users.random()
	beforeAll(done => {
		byrds.signup(data).then(u => {
			user = u
			data.id = user.userData.id
		}).then(_ => {
			return user.orgs.create(org)
		}).then(o => {
			org.id = o.id
		}).then(done)
	})

	crudTests.expectCRUD(_ => user.org(org).groups)

	it ('should return correct output', done => {
		var group = conf.groups.random('foo')
		user.org(org).groups.create(group).then(g => {
			expect(typeof g).toEqual('object')
			expect(g.name).toEqual(group.name)
			expect(g.organization.id || g.organization).toEqual(org.id)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	// it ('should not return unsafe properties', done => {
	// 	var group = conf.groups.random()
	// 	user.org(org).groups.create(group).then(g => {
	// 		// expect(g.secretKey).toBeUndefined()
	// 	}).catch(err => {
	// 		/* istanbul ignore next */
	// 		expect(err.status).toEqual(200, err)
	// 	}).then(done)
	// })

})
