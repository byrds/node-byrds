const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const byrds = Byrds(conf.apiKeys.client)

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

describe('user.org(:id).groups', () => {

	var user = undefined
	var groupsIds = undefined
	var groupsCount = getRandomInt(5, 15)
	var specialGroups = [ 'all', 'admins' ]
	var org = conf.orgs.random()
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
		}).then(_ => {
			return user.orgs.create(org)
		}).then(o => {
			org.id = o.id
		}).then(_ => {
			return conf.groups.insertMany(user.org(org), groupsCount)
		}).then(_ => {
			return user.org(org).groups.read({ pageSize: 500 })
		}).then(groups => {
			groupsIds = groups.map(d => d.id)
		}).then(done)
	})

	crudTests.expectCRUD(_ => user.org(org).groups, _ => groupsIds)

	describe('.read()', () => {

		it ('should return correct output (' + (groupsCount + specialGroups.length) + ' groups)', done => {
			user.org(org).groups.read({ pageSize: 500 }).then(groups => {
				expect(groups instanceof Array).toBe(true)
				expect(groups.length).toEqual(groupsCount + specialGroups.length)

				var g = groups[0]
				expect(g.id).toBeDefined()
			}).catch(err => {
			/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('should return organization\'s groups only', done => {
			user.org(org).groups.read().then(groups => {
				groups.forEach(g => expect(g.organization.id || g.organization).toEqual(org.id))
			}).catch(err => {
			/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

	})

})

describe('user.group(:id)', () => {

	var user = undefined
	var org = conf.orgs.random()
	var group = conf.groups.random()
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
		}).then(_ => {
			return user.orgs.create(org)
		}).then(o => {
			org.id = o.id
		}).then(_ => {
			return user.org(org).groups.create(group)
		}).then(g => {
			group.id = g.id
		}).then(done)
	})

	it ('.get(:id) should return the correct group', done => {
		user.groups.get(group.id).then(g => {
			expect(typeof g).toEqual('object')

			expect(g.id).toEqual(group.id)
			expect(g.name).toEqual(group.name)
			expect(g.organization.id || g.organization).toEqual(org.id)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
