const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const byrds = Byrds(conf.apiKeys.client)

describe('user.group(:id)', () => {

	var user = undefined
	var org = conf.orgs.random()
	var group = conf.groups.random()
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
			return user.orgs.create(org)
		}).then(o => {
			org.id = o.id
			return user.org(org).groups.create(group)
		}).then(g => {
			group.id = g.id
		}).then(done)
	})

	it ('.edit(:id, data) should return the updated group', done => {
		var data = { name: 'test' }
		user.groups.edit(group.id, data).then(g => {
			expect(typeof g).toEqual('object')

			expect(g.id).toEqual(group.id)
			expect(g.name).toEqual(data.name)
			expect(g.organization.id || g.organization).toEqual(org.id)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
