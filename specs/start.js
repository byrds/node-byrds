'use strict'

var path = require('path')
var Jasmine = require('jasmine')
var SpecReporter = require('jasmine-spec-reporter')
var argv = require('minimist')(process.argv.slice(2))

var jasmine = new Jasmine()
jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000
jasmine.configureDefaultReporter({ print: _ => {} })
jasmine.addReporter(new SpecReporter({
	displayStacktrace: 'all',
	displaySpecDuration: true,
	displayPendingSpec: true,
	displayPendingSummary: false,
}))
jasmine.loadConfigFile(path.join(__dirname, 'support/jasmine.json'))

jasmine.onComplete(function (passed) {
	process.exit(passed ? 0 : 1)
})

var filesToRun = argv ? argv._ : undefined
jasmine.execute(filesToRun)
