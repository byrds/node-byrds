const randomkey = require('randomkey')

const md5 = require('md5')
const passwordPrefix = '0A0A110A-289A-491D-B36C-A5731253F2C9'

function userData() {
	var name = randomkey(16)
	return {
		name: name,
		email: name + '@byrds.io',
		password: name,
		_pwd: md5(passwordPrefix + name)
	}
}

module.exports = {
	random: userData
}