const overflow = 5

var expectCrud = obj => {
	expect(typeof obj.create).toEqual('function')
	expect(typeof obj.read).toEqual('function')
	expect(typeof obj.update).toEqual('function')
	expect(typeof obj.delete).toEqual('function')
}

var expectAliases = obj => {
	expect(typeof obj.new).toEqual('function')
	expect(typeof obj.add).toEqual('function')
	expect(typeof obj.insert).toEqual('function')
	expect(typeof obj.get).toEqual('function')
	expect(typeof obj.list).toEqual('function')
	expect(typeof obj.remove).toEqual('function')
}

exports.methods = api => {
	describe('.get(:path, :options)', () => {

		it ('should send a GET request with params', done => {
			const params = { foo: 'bar' }
			api.get('/ci/methods', { params: params }).then(info => {
				expect(info.method).toEqual('GET')
				expect(info.params.foo).toEqual(params.foo)
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('should be able to catch errors', done => {
			api.get('/ci/error').then(info => {
				/* istanbul ignore next */
				throw new Error('Returned 200')
			}).catch(err => {
				expect(err.status).toEqual(400, err)
			}).then(done)
		})

	})

	describe('.post(:path, :options)', () => {

		it ('should send a POST request with body', done => {
			const body = { foo: 'bar' }
			api.post('/ci/methods', body).then(info => {
				expect(info.method).toEqual('POST')
				expect(info.body.foo).toEqual(body.foo)
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('should be able to catch errors', done => {
			api.post('/ci/error').then(info => {
				/* istanbul ignore next */
				throw new Error('Returned 200')
			}).catch(err => {
				expect(err.status).toEqual(400, err)
			}).then(done)
		})

	})

	describe('.put(:path, :options)', () => {

		it ('should send a PUT request with body', done => {
			const body = { foo: 'bar' }
			api.put('/ci/methods', body).then(info => {
				expect(info.method).toEqual('PUT')
				expect(info.body.foo).toEqual(body.foo)
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('should be able to catch errors', done => {
			api.put('/ci/error').then(info => {
				/* istanbul ignore next */
				throw new Error('Returned 200')
			}).catch(err => {
				expect(err.status).toEqual(400, err)
			}).then(done)
		})

	})

	describe('.delete(:path, :options)', () => {

		it ('should send a DELETE request with params', done => {
			const params = { foo: 'bar' }
			api.delete('/ci/methods', { params: params }).then(info => {
				expect(info.method).toEqual('DELETE')
				expect(info.params.foo).toEqual(params.foo)
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		it ('should be able to catch errors', done => {
			api.delete('/ci/error').then(info => {
				/* istanbul ignore next */
				throw new Error('Returned 200')
			}).catch(err => {
				expect(err.status).toEqual(400, err)
			}).then(done)
		})

	})
}

exports.expectCRUD = (crudInterface, objectIds) => {
	crudInterface = (typeof crudInterface === 'function') ? crudInterface : _ => crudInterface

	describe('CRUD', () => {
		it ('should return a CRUD interface', () => {
			expectCrud(crudInterface())
		})

		it ('should return CRUD aliases', () => {
			expectAliases(crudInterface())
		})

		it ('methods should return Promises', () => {
			const p = crudInterface().read()
			expect(typeof p.then).toEqual('function')
		})

		if (objectIds) {
			objectIds = (typeof objectIds === 'function') ? objectIds : _ => objectIds
			
			it ('readOne(id) should return the matching object', done => {
				const ids = objectIds()
				Promise.all(ids.map(id => {
					return crudInterface().readOne(id).then(result => {
						expect(result.id).toEqual(id, '(id: ' + id + ')')
					}).catch(err => {
						/* istanbul ignore next */
						expect(err.status).toEqual(200, err)
					})
				})).then(done)
			})
			
			it ('pageSize(n) should return n objects', done => {
				const ids = objectIds()
				const params = Array.from(Array(ids.length + (2 * overflow)).keys()).map(n => n - overflow) // [ 0, 1, 2, ..., n ]
				Promise.all(params.map(n => {
					const expectedLength = n ? Math.min(n, ids.length) : ids.length

					return crudInterface().read({
						pageSize: n
					}).then(results => {
						/* istanbul ignore if */
						if (n < 0) throw new Error('Returned 200') // Should return a 400 on negative pageSize
						expect(results.length).toEqual(expectedLength, '(pageSize: ' + n + ')')
						expect(results[0].id).toEqual(ids[0], '(pageSize: ' + n + ')')
					}).catch(err => {
						expect(err.status).toEqual(400, '(pageSize: ' + n + ')')
						expect(err.data.error).toEqual('Page/PageSize query attributes are invalid')
					})
				})).then(done)
			})

			it ('page(n) should return the nth page', done => {
				const ids = objectIds()
				const params = Array.from(Array(ids.length + (2 * overflow)).keys()) // [ 0, 1, 2, ..., n ]
				Promise.all(params.map(n => {
					const page = n - overflow
					return crudInterface().read({
						page: page,
						pageSize: 1
					}).then(results => {
						/* istanbul ignore if */
						if (page < 0) throw new Error('Returned 200') // Should return a 400 on negative page
						if (page >= 0 && page < ids.length) expect(results[0].id).toEqual(ids[page], '(page: ' + page + ')')
						else expect(results.length).toEqual(0, '(page: ' + page + ')')
					}).catch(err => {
						expect(err.status).toEqual(400, '(page: ' + page + ')')
						expect(err.data.error).toEqual('Page/PageSize query attributes are invalid')
					})
				})).then(done)
			})
		}
	})
}
