const randomkey = require('randomkey')

function orgData(name) {
	if (!name) name = randomkey(16)
	return {
		name: name,
	}
}

function insertMany(user, orgCount) {
	var promises = []
	for (var i=0, k=orgCount; i<k; i++) {
		promises.push(user.orgs.create(orgData()))
	}
	return Promise.all(promises)
}

module.exports = {
	random: orgData,
	insertMany: insertMany
}