const randomkey = require('randomkey')

function deviceData() {
	var serial = randomkey(16)
	return {
		serial: serial,
		udid: serial,
		meta: {
			DeviceName: serial,
		}
	}
}

function insertMany(user, deviceCount) {
	var promises = []
	for (var i=0, k=deviceCount; i<k; i++) {
		promises.push(user.devices.create(deviceData()))
	}
	return Promise.all(promises)
}

module.exports = {
	random: deviceData,
	insertMany: insertMany
}