const randomkey = require('randomkey')

function groupData(name) {
	if (!name) name = randomkey(16)
	return {
		name: name,
	}
}

function insertMany(user, groupCount) {
	var promises = []
	for (var i=0, k=groupCount; i<k; i++) {
		promises.push(user.groups.create(groupData()))
	}
	return Promise.all(promises)
}

module.exports = {
	random: groupData,
	insertMany: insertMany
}