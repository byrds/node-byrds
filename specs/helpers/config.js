const randomkey = require('randomkey')

const orgs = require('./orgs')
const users = require('./users')
const groups = require('./groups')
const devices = require('./devices')

module.exports = {
	orgs: orgs,
	users: users,
	groups: groups,
	devices: devices,
	
	apiKeys: {
		app: 'app',
		enterprise: 'enterprise',
		client: 'client',
	},
}
