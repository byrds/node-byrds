const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const byrds = Byrds(conf.apiKeys.client)

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

describe('orgs', () => {

	var user = undefined
	var orgsIds = undefined
	var orgsCount = getRandomInt(5, 15)
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
		}).then(_ => {
			return conf.orgs.insertMany(user, orgsCount)
		}).then(_ => {
			return user.orgs.read({ pageSize: 500 })
		}).then(orgs => {
			orgsIds = orgs.map(o => o.id)
		}).then(done)
	})

	crudTests.expectCRUD(_ => user.orgs, _ => orgsIds)

	describe('.read()', () => {

		it ('should return correct output (' + orgsCount + ' orgs)', done => {
			user.orgs.read({ pageSize: 500 }).then(orgs => {
				expect(orgs instanceof Array).toBe(true)
				expect(orgs.length).toEqual(orgsCount + 1) // + User org

				var o = orgs[0]
				expect(o.id).toBeDefined()
			}).catch(err => {
			/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

		xit ('should return user\'s orgs only', done => {
			user.orgs.read().then(orgs => {
				orgs.forEach(o => expect(o.user.id).toEqual(user.userData.id))
			}).catch(err => {
			/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

	})

})

describe('org(:id)', () => {

	var user = undefined
	const org = conf.orgs.random()
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
			return user.orgs.create(org)
		}).then(o => {
			org.id = o.id
		}).then(done)
	})

	it ('.get(:id) should return the correct org', done => {
		user.orgs.get(org.id).then(o => {
			expect(typeof o).toEqual('object')

			expect(o.id).toEqual(org.id)
			expect(o.name).toEqual(org.name)
			expect(o.type).toEqual('ORGANIZATION_TYPE_CLASSIC')
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
