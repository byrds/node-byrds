const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const org = Byrds(conf.apiKeys.enterprise)

describe('org.devices', () => {

	// crudTests.expectCRUD(org.devices)

	xit ('.read() should return a device', done => {
		org.devices.read().then(devices => {
			expect(devices instanceof Array).toBe(true)
			expect(devices.length).toEqual(1)

			var d = devices[0]
			expect(d.user.id).toEqual(user.data.id)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
