const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const byrds = Byrds(conf.apiKeys.client)

describe('orgs.create()', () => {

	var user = undefined
	const data = conf.users.random()
	beforeAll(done => {
		byrds.signup(data).then(u => {
			user = u
			data.id = user.userData.id
		}).then(done)
	})

	crudTests.expectCRUD(_ => user.orgs)

	it ('should return correct output', done => {
		var org = conf.orgs.random('foo')
		user.orgs.create(org).then(o => {
			expect(typeof o).toEqual('object')
			expect(o.name).toEqual(org.name)
			expect(o.type).toEqual('ORGANIZATION_TYPE_CLASSIC')
			expect(typeof o.clientKey).toEqual('string')
			expect(o.clientKey.length).toEqual(64)
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

	it ('should not return unsafe properties', done => {
		var org = conf.orgs.random()
		user.orgs.create(org).then(o => {
			expect(o.secretKey).toBeUndefined()
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
