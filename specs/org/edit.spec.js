const Byrds = require('../../main')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

const byrds = Byrds(conf.apiKeys.client)

describe('user.org(:id)', () => {

	var user = undefined
	var org = conf.orgs.random('foo')
	beforeAll(done => {
		const data = conf.users.random()
		const byrds = Byrds(conf.apiKeys.client)
		byrds.signup(data).then(u => {
			user = u
			return user.orgs.create(org)
		}).then(o => {
			org.id = o.id
		}).then(done)
	})

	it ('.edit(:id, data) should return the updated org', done => {
		var data = { name: 'test' }
		user.orgs.edit(org.id, data).then(o => {
			expect(typeof o).toEqual('object')

			expect(o.id).toEqual(org.id)
			expect(o.name).toEqual(data.name)
			expect(o.type).toEqual('ORGANIZATION_TYPE_CLASSIC')
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
