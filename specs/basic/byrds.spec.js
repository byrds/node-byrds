const Byrds = require('../../main.js')
const conf = require('../helpers/config')
const crudTests = require('../helpers/crudTests')

function testLibraryBasics(byrds) {

	it ('should not expose internals by default', () => {
		expect(byrds.api).toBeUndefined()
		expect(byrds.context).toBeUndefined()
	})

	it ('should now expose an api object', () => {
		var withInternal = byrds.withContext({ _exposeInternal: true })
		expect(typeof withInternal.api).toEqual('object')
	})

	it ('should now have a context', () => {
		var withInternal = byrds.withContext({ _exposeInternal: true })
		expect(typeof withInternal).toEqual('object')
		expect(withInternal.withContext({ test: true }).context.test).toBe(true)
	})

	describe('.api', () => {
		var api = byrds.withContext({ _exposeInternal: true }).api

		it ('should have a conf object', () => {
			expect(typeof api.conf).toEqual('object')
		})

		it ('should have an App Key', () => {
			expect(typeof api.conf.apiKey).toEqual('string')
			expect(api.conf.apiKey).toEqual(conf.apiKeys.client)
		})

		it ('should get the App Key from ENV if not provided', () => {
			var b = Byrds().withContext({ _exposeInternal: true })
			expect(typeof b.api.conf.apiKey).toEqual('string')
			expect(b.api.conf.apiKey).toEqual(process.env['BYRDS_API_KEY'])
		})

		crudTests.methods(api)

	})

	describe('.full()', () => {
		var api = byrds.withContext({ _exposeInternal: true }).api

		it ('should return full response object', done => {
			byrds.full().reach().then(result => {
				expect(typeof result.context).toEqual('object')
				expect(typeof result.data).toEqual('object')
				expect(result.context.href).toEqual((api.host + (api.subPath || '')))
			}).catch(err => {
				/* istanbul ignore next */
				expect(err.status).toEqual(200, err)
			}).then(done)
		})

	})

	describe('conf: needsAuthCallback', () => {

		it ('should be triggered when accessing api without access', done => {
			var b = Byrds({
				needsAuthCallback: err => {
					expect(err.status).toEqual(401, err)
					expect(err.data.error).toEqual('Invalid auth (jwt) token', err)
					done()
				}
			}).withContext({ userToken: 'INVALID_TOKEN' })

			b.me.info()
		})

	})

}


describe('byrds library', () => testLibraryBasics(Byrds(conf.apiKeys.client)))
describe('byrds library: cli mode', () => testLibraryBasics(Byrds(conf.apiKeys.client).withContext({ cli: true })))
