const Byrds = require('../../main.js')
const conf = require('../helpers/config')

const byrds = Byrds(conf.apiKeys.client)

describe('byrds.reach()', () => {
	it ('should return a Promise object', () => {
		const p = byrds.reach()
		expect(typeof p.then).toEqual('function')
	})

	it ('should return correct output', done => {
		byrds.reach().then(data => {
			expect(data.title).toEqual('Byrds_MDM_API')
			expect(data.version).toEqual('0.1.0')
			expect(data.env).toEqual('test')
		}).catch(err => {
			/* istanbul ignore next */
			expect(err.status).toEqual(200, err)
		}).then(done)
	})

})
