var extend = require('lodash/fp/extend')
var cli = undefined
if (process.env['BYRDS_SDK_TARGET'] !== 'browser') {
	try {
		cli = require('cli')
	} catch (e) {
		// Do nothing
	}
}

module.exports = (conf, context) => {
	var app = {
		cmds: {},
		conf: conf,
		context: context || {}
	}
	app.api = require('./helpers/api')(app, conf, cli)

	var files = []
	/* istanbul ignore if  */
	if (process.env['BYRDS_SDK_TARGET'] === 'browser') {
		app.debug = conf.debug ? console.log.bind(console, '[byrds]') : _ => {}
		require('./cmds/*.js', { glob: true }) // Preload modules, so browerify can process them
		files = [
			'_asUser',
			'_cli',
			'_full',
			'_isAuthenticated',
			'_logout',
			'_version',
			'_withContext',
			'commands',
			'device',
			'devices',
			'group',
			'groups',
			'login',
			'me',
			'org',
			'orgs',
			'policies',
			'reach',
			'retire',
			'signup',
			'twoFactorAuth',
			'users',
		]
	} else {
		app.debug = require('debug')('byrds')
		files = require('./helpers/fetchFiles').list('../cmds')
	}
	files.forEach(f => {
		var cmd = require('./cmds/' + f + '.js')
		if (typeof cmd === 'function') {
			app.cmds[f] = cmd(app)
		} else {
			app.cmds[cmd.name || f] = cmd.cmd(app)
		}
	})

	// Not exposed by default
	if (app.context._exposeInternal) {
		app.cmds.api = app.api
		app.cmds.context = app.context
	}

	return app.cmds
}