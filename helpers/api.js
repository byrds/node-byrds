var colors = require('colors/safe')
var keypath = require('nasa-keypath')
var axios = require('axios')

var req = {
	get: options => {
		options.method = 'GET'
		return axios.request(options)
	},
	post: options => {
		options.method = 'POST'
		return axios.request(options)
	},
	put: options => {
		options.method = 'PUT'
		return axios.request(options)
	},
	delete: options => {
		options.method = 'DELETE'
		return axios.request(options)
	},
}

function crud(app, buildURI, buildOptions, callback) {
	var methods = {
		create: data => {
			return app.api.post(buildURI(app), data).then(r => {
				if (callback) callback(r)
				return r
			})
		},
		read: (query) => {
			return app.api.get(buildURI(app), buildOptions(app, query || {})).then(r => {
				if (callback) callback(r)
				return r
			})
		},
		readOne: (obj, query) => {
			return app.api.get(buildURI(app, obj), buildOptions(app, query)).then(r => {
				if (callback) callback(r)
				return r
			})
		},
		update: (obj, data) => {
			return app.api.put(buildURI(app, obj), data).then(r => {
				if (callback) callback(r)
				return r
			})
		},
		delete: obj => {
			return app.api.delete(buildURI(app, obj)).then(r => {
				if (callback) callback(r)
				return r
			})
		},
	}

	// Aliases
	methods.add = methods.create
	methods.new = methods.create
	methods.insert = methods.create
	methods.list = methods.read
	methods.get = methods.readOne
	methods.edit = methods.update
	methods.remove = methods.delete
	
	return methods
}


module.exports = (app, conf, cli) => {
	if (typeof conf !== 'object') {
		conf = { apiKey: conf }
	}
	conf.apiKey = conf.apiKey || process.env['BYRDS_API_KEY']
	var self = {
		host: conf.host || process.env['BYRDS_HOST'] || 'https://api.byrds.io',
		subPath: conf.subPath || process.env['BYRDS_SUBPATH'] || '/api',
		conf: conf,

		buildOptions: (path, options, data) => {
			options = options || {}
			keypath.set(options, 'url', (self.subPath || '') + (path || ''))
			keypath.set(options, 'baseURL', self.host)
			keypath.set(options, 'headers.application-key', self.conf.apiKey)
			keypath.set(options, 'headers.authorization', app.context.userToken)
			keypath.set(options, 'data', data)
			keypath.set(options, 'params', options.qs)

			// keypath.set(options, 'resolveWithFullResponse', true)
			// keypath.set(options, 'json', true)
			
			// if (options.headers) app.debug('headers:', JSON.stringify(options.headers, undefined, 4))
			if (options.qs) app.debug('query:', JSON.stringify(options.qs, undefined, 4))
			if (options.data) app.debug('body:', JSON.stringify(options.data, undefined, 4))
			return options
		},
		buildResponse: (res, options) => {
			if (!self.conf.fullResponse && !app.context.fullResponse) return res.data

			options = options || {}
			var r = {
				data: res.data,
				context: {
					href: res.config.url
				}
			}
			if (keypath.get(options, 'qs.page')) r.context.page = options.qs.page
			if (res.data.totalCount) r.context.totalCount = res.data.totalCount
			if (res.headers.page) r.context.page = res.headers.page
			if (res.headers['total-count']) r.context.totalCount = res.headers['total-count']
			return r
		},
		authCallback: function(err) {
			if (err.status === 401 && err.data && err.data.error === 'Invalid auth (jwt) token' && conf.needsAuthCallback) {
				conf.needsAuthCallback(err)
			}
		},

		crud: crud,

		get: (path, options) => {
			app.debug(colors.bold('GET ') + colors.gray(self.host) + colors.cyan((self.subPath || '') + (path || '')))
			if (app.context.cli) cli.spinner('Request in progress...')
			var startTime = new Date
			return req.get(self.buildOptions(path, options)).then(res => {
				var responseTime = (new Date - startTime).toString() + 'ms'
				if (app.context.cli) cli.spinner('Request in progress... ' + responseTime, true)
				return self.buildResponse(res, options)
			}).catch(err => {
				var responseTime = (new Date - startTime).toString() + 'ms'
				if (app.context.cli) cli.spinner('Request in progress... ' + responseTime, true)
				app.debug('Error reaching Byrds MDM API:', err.statusMessage || err.data || err.statusText || 'Unknown')
				self.authCallback(err)
				throw err
			})
		},
		post: (path, data, options) => {
			app.debug(colors.bold('POST ') + colors.gray(self.host) + colors.cyan((self.subPath || '') + (path || '')))
			if (app.context.cli) cli.spinner('Request in progress...')
			var startTime = new Date
			return req.post(self.buildOptions(path, options, data)).then(res => {
				var responseTime = (new Date - startTime).toString() + 'ms'
				if (app.context.cli) cli.spinner('Request in progress... ' + responseTime, true)
				return self.buildResponse(res, options)
			}).catch(err => {
				if (app.context.cli) cli.spinner('Request in progress... done!', true)
				app.debug('Error reaching Byrds MDM API:', err.statusMessage || err.data || err.statusText || 'Unknown')
				self.authCallback(err)
				throw err
			})
		},
		put: (path, data, options) => {
			app.debug(colors.bold('PUT ') + colors.gray(self.host) + colors.cyan((self.subPath || '') + (path || '')))
			if (app.context.cli) cli.spinner('Request in progress...')
			var startTime = new Date
			return req.put(self.buildOptions(path, options, data)).then(res => {
				var responseTime = (new Date - startTime).toString() + 'ms'
				if (app.context.cli) cli.spinner('Request in progress... ' + responseTime, true)
				return self.buildResponse(res, options)
			}).catch(err => {
				if (app.context.cli) cli.spinner('Request in progress... done!', true)
				app.debug('Error reaching Byrds MDM API:', err.statusMessage || err.data || err.statusText || 'Unknown')
				self.authCallback(err)
				throw err
			})
		},
		delete: (path, options) => {
			app.debug(colors.bold('DELETE ') + colors.gray(self.host) + colors.cyan((self.subPath || '') + (path || '')))
			if (app.context.cli) cli.spinner('Request in progress...')
			var startTime = new Date
			return req.delete(self.buildOptions(path, options)).then(res => {
				var responseTime = (new Date - startTime).toString() + 'ms'
				if (app.context.cli) cli.spinner('Request in progress... ' + responseTime, true)
				return self.buildResponse(res, options)
			}).catch(err => {
				var responseTime = (new Date - startTime).toString() + 'ms'
				if (app.context.cli) cli.spinner('Request in progress... ' + responseTime, true)
				app.debug('Error reaching Byrds MDM API:', err.statusMessage || err.data || err.statusText || 'Unknown')
				self.authCallback(err)
				throw err
			})
		},

		logResult: response => {
			var data = response
			if (app.context.fullResponse) {
				data = response.data
				app.debug('response info:', response.info)
			}
			if (data && data instanceof Array) {
				app.debug('objects returned:', data.map(o => { return o.id }))
			} else if (data && data.id) {
				app.debug('object returned:', data.id)
			}
		}
	}
	return self
}
