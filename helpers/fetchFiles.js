var fs = require('fs')
var join = require('path').join

function endsWith(str, suffix) {
	return str.indexOf(suffix, str.length - suffix.length) !== -1
}

exports.list = function(folder) {
	var dir = (folder.indexOf('.') === 0) ? join(__dirname, folder) : folder

	var outFiles = []

	var files = fs.readdirSync(dir + '/')
	for (var i in files) {
		var f = files[i]
		var fn = f.substring(f.lastIndexOf('/') + 1)
		try {
			var stats = fs.statSync(join(dir, fn))
			if (stats.isFile() && endsWith(fn, '.js')) {
				var table = fn.substring(0, fn.lastIndexOf('.js'))
				outFiles.push(table)
			}
		} catch(ex) {
			/* istanbul ignore next */
			console.error(ex)
		}
	}

	return outFiles
}
