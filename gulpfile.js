var gulp = require('gulp');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

gulp.task('build', function() {
    return browserify({ standalone: 'Byrds', entries: 'main.js', debug: true, fullPaths: true })
    	.ignore('request')
    	.ignore('cls-bluebird')
        .transform('babelify', { presets: [ 'es2015' ] })
        .transform('envify', { BYRDS_SDK_TARGET: 'browser' })
		.transform('require-globify')
        .bundle()
        .pipe(source('byrds.js'))
        .pipe(buffer())
	    .pipe(sourcemaps.init())
        .pipe(gulp.dest('dist'))
        .pipe(uglify())
	    .pipe(rename({ extname: '.min.js' }))
	    .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['build'], function() {
    gulp.watch('*.js', ['build']);
});

gulp.task('default', ['watch']);
