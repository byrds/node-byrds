exports.name = 'cli'
exports.cmd = app => {
	return _ => {
		return app.cmds.withContext({ cli: true })
	}
}
