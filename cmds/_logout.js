exports.name = 'logout'
exports.cmd = app => {
	return _ => {
		delete app.context.userToken
		delete app.user
	}
}
