exports.name = 'reach'
exports.cmd = app => {
	return _ => {
		return app.api.get().then(info => {
			app.debug('Reached:', (info ? true : false))
			return info
		})
	}
}