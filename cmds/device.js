module.exports = app => {
	return (device) => {
		return app.cmds.withContext({
			device: device
		})
	}
}