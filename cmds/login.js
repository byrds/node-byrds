const md5 = require('md5')

const passwordPrefix = '0A0A110A-289A-491D-B36C-A5731253F2C9'

module.exports = app => {
	return (data, options) => {
		return app.api.post('/auth', {
			user: data.email,
			password: md5(passwordPrefix + data.password),
			lifetime: data.lifetime,
			token: data.token
		}, options).then(response => {
			app.debug('user returned:', response.user)
			return app.cmds.asUser(response)
		})
	}
}