const md5 = require('md5')

const passwordPrefix = '0A0A110A-289A-491D-B36C-A5731253F2C9'

exports.name = 'me'
exports.cmd = app => {
	return {
		info: options => {
			return app.api.get('/me', options)
		},
		edit: (data, options) => {
			var d = {
				name: data.name,
				email: data.email,
				token: data.token,
			}
			if (data.password) d.password = md5(passwordPrefix + data.password)
			if (data.currentPassword) d.currentPassword = md5(passwordPrefix + data.currentPassword)
			return app.api.put('/me', d, options)
		},
		delete: options => {
			return app.api.delete('/me', options)
		}
	}
}