const version = require('../package.json').version

exports.name = 'version'
exports.cmd = app => {
	return _ => {
		return version
	}
}
