exports.name = 'isAuthenticated'
exports.cmd = app => {
	return _ => {
		return (app.context.userToken !== undefined)
	}
}
