const Byrds = require('../main')
const extend = require('lodash/fp/extend')

exports.name = 'withContext'
exports.cmd = app => {
	return data => {
		var newContext = extend({}, app.context || {})
		newContext = extend(newContext, data || {})
		return Byrds(app.conf, newContext)
	}
}
