exports.name = 'full'
exports.cmd = app => {
	return _ => {
		return app.cmds.withContext({ fullResponse: true })
	}
}