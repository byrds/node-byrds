module.exports = app => {
	return _ => {
		return app.api.get('/devices/' + app.context.device + '/retire').then(data => {
			app.debug('The device was retired:', data.acknowledged)
			return data
		})
	}
}
