function buildURI(app, user) {
	if (app.context.group) {
		var group = app.context.group.id || app.context.group
		if (user) return '/groups/' + group + '/users/' + (user.id || user)
		return '/groups/' + group + '/users'
	}
	if (user) return '/users/' + (user.id || user)
	if (app.context.organization) {
		var organization = app.context.organization.id || app.context.organization
		return '/organizations/' + organization + '/users'
	}
	return '/users'
}

function buildOptions(app, query) {
	var options = {
		qs: query
	}
	return options
}

module.exports = app => {
	return app.api.crud(app, buildURI, buildOptions, app.api.logResult)
}
