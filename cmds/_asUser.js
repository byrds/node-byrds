const extend = require('lodash/fp/extend')

exports.name = 'asUser'
exports.cmd = app => {
	return user => {
		if (typeof user === 'string') user = { token: user }
		return extend(app.cmds.withContext({ userToken: user.token }), { userData: user.user })
	}
}