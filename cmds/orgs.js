function buildURI(app, organization) {
	if (organization) return '/organizations/' + (organization.id || organization)
	if (app.context.organization) {
		var organization = app.context.organization.id || app.context.organization
		return '/organizations/' + organization + '/devices'
	}
	return '/organizations'
}

function buildOptions(app, query) {
	var options = {
		qs: query
	}
	return options
}

module.exports = app => {
	return app.api.crud(app, buildURI, buildOptions, app.api.logResult)
}
