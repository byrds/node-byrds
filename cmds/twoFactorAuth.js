const extend = require('lodash/fp/extend')

exports.name = 'twoFactorAuth'
exports.cmd = app => {

	var authy = {
		register: data => {
			return app.api.post('/me/2fa/register/authy', {
				email: data.email,
				phone: data.phone,
				countryCode: data.countryCode,
			})
		},
		validate: token => {
			return app.api.post('/me/2fa/validate/authy', {
				token: token,
			})
		}
	}

	return {
		authy: authy,
		disable: _ => app.api.delete('/me/2fa')
	}
}