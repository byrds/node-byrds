function buildURI(app, group) {
	if (group) return '/groups/' + (group.id || group)
	// if (app.context.user) {
	// 	var user = app.context.user.id || app.context.user
	// 	return '/users/' + user + '/groups'
	// }
	if (app.context.group) {
		var group = app.context.group.id || app.context.group
		return '/groups/' + group
	}
	if (app.context.organization) {
		var organization = app.context.organization.id || app.context.organization
		return '/organizations/' + organization + '/groups'
	}
	return '/groups'
}

function buildOptions(app, query) {
	var options = {
		qs: query
	}
	return options
}

module.exports = app => {
	return app.api.crud(app, buildURI, buildOptions, app.api.logResult)
}
