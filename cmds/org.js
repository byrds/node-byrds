module.exports = app => {
	return (organization) => {
		return app.cmds.withContext({
			organization: organization
		})
	}
}