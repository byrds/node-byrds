function buildURI(app, command) {
	if (command) return '/commands/' + (command.id || command)
	// if (app.context.group) {
	// 	var group = app.context.group.id || app.context.group
	// 	// return '/groups/' + group + '/commands'
	// 	return '/groupCommands/' + group
	// }
	// if (app.context.device) {
	// 	var device = app.context.device.id || app.context.device
	// 	// return '/devices/' + device + '/commands'
	// 	return '/deviceCommands/' + device
	// }
	// if (app.context.organization) {
	// 	var organization = app.context.organization.id || app.context.organization
	// 	// return '/organizations/' + organization + '/commands'
	// 	return '/organizationCommands/' + organization
	// }
	return '/commands'
}

function buildOptions(app, query) {
	var options = {
		qs: query
	}
	if (app.context.group) {
		options.qs.group = app.context.group.id || app.context.group._id || app.context.group
	}
	if (app.context.device) {
		options.qs.device = app.context.device.id || app.context.device._id || app.context.device
	}
	if (app.context.organization) {
		options.qs.organization = app.context.organization.id || app.context.organization._id || app.context.organization
	}
	return options
}

module.exports = app => {
	return app.api.crud(app, buildURI, buildOptions, app.api.logResult)
}
