function buildURI(app, policy) {
	// if (policy) {
	// 	return '/policies/' + policy
	// }
	// if (app.context.group) {
	// 	var group = app.context.group.id || app.context.group
	// 	// return '/groups/' + group + '/policies'
	// 	return '/groupPolicies/' + group
	// }
	// if (app.context.device) {
	// 	var device = app.context.device.id || app.context.device
	// 	// return '/devices/' + device + '/policies'
	// 	return '/devicePolicies/' + device
	// }
	if (app.context.organization) {
		var organization = app.context.organization.id || app.context.organization
		return '/organizations/' + organization + '/policy'
		// return '/organizationPolicies/' + organization
	}
	return '/organizationPolicies'
}

function buildOptions(app, query) {
	var options = {
		qs: query
	}
	// if (policy) {
	// 	options.qs.policy = policy.id || policy._id || policy
	// }
	return options
}

module.exports = app => {
	var policies = app.api.crud(app, buildURI, buildOptions, app.api.logResult)
	policies.types = _ => {
		return app.api.get('/policies/types')
	}
	return policies
}
