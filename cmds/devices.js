function buildURI(app, device) {
	if (device) return '/devices/' + (device.id || device)
	if (app.context.device) return '/devices/' + app.context.device
	if (app.context.organization) {
		var organization = app.context.organization.id || app.context.organization
		return '/organizations/' + organization + '/devices'
	}
	return '/devices'
}

function buildOptions(app, query) {
	var options = {
		qs: query
	}
	return options
}

module.exports = app => {
	return app.api.crud(app, buildURI, buildOptions, app.api.logResult)
}
