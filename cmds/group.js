module.exports = app => {
	return (group) => {
		return app.cmds.withContext({
			group: group
		})
	}
}